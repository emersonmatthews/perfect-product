# The Perfect Product

This website is a simple (read: vanilla) website describing the perfect product. It's heavily inspired by a song, The Perfect Product by TWRP. I am not affiliated to TWRP in any way.

## Deploy

This website's deployment to https://product.recurse.me is handled by Netlify automatically whenever you push to master.
